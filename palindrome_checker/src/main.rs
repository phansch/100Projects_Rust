fn main() {
    let string: &str = "regallager";
    println!("{}", is_palindrome(string));
}

fn is_palindrome(val: &str) -> bool {
    string_reverse(val) == val
}

fn string_reverse(val: &str) -> String {
    val.chars().rev().collect()
}

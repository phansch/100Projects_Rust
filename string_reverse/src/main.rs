fn main() {
    let string: &str = "Hello World!";
    println!("{}", string_reverse(string));
}

fn string_reverse(val: &str) -> String {
    val.chars().rev().collect()
}

use std::io;

struct FibonacciCounter {
    curr: u32,
    next: u32,
}

impl FibonacciCounter {
    fn new() -> FibonacciCounter {
        FibonacciCounter {
            curr: 1,
            next: 1
        }
    }
}

impl Iterator for FibonacciCounter {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        // Mostly taken from http://rustbyexample.com/trait/iter.html
        let new_next = self.curr + self.next;
        self.curr = self.next;
        self.next = new_next;

        Some(self.curr)
    }
}

fn main() {
    println!("Enter the amount of Fibonacci Numbers you want to have:");

    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .ok()
        .expect("Failed to read line");

    let limit: usize = line.trim().parse().ok().expect("Wanted a number");

    println!("--- Printing the first 5 Fibonacci numbers:");
    let fibonacci_counter = FibonacciCounter::new();
    for x in fibonacci_counter.take(limit) {
        println!("{}", x);
    }
}
